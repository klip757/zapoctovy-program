﻿using System;
using System.CodeDom;
using System.Windows;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSHra
{

    abstract class Member
    {
        //public abstract Bitmap MujObrazek();
        public Rectangle Shape = new Rectangle();
        
        public Mapa mapa;
        //public int x; 
        //public int y;
        public char direction;
        public int speed;
        public int cons = 10;
        public int jumpConst = 30;
        public int maxSpeed = 15;
        public int speedup = 0;
        public int jump;
        public int consY = 10;
        public int maxSpeedUp = 10;
        public bool moved = false;
        public bool movedLetter = false;
        public int num = 50;
        public bool switchJump;
        public Random rand = new Random();


    }

    class MovingMember : Member
    {
        /*
        public Mapa mapa;
        public int x; 
        public int y;
        public int direction;
        public int speed;
        public int cons;
        public int maxSpeed = 10;
        public int speedup;
        public int jump;
        */
        public int oldX, oldY;

        public bool letterBool = false;

        private static char[] dirArray = {'>', '^', '<', 'v'};

        public virtual void DoStep()
        {
        }

        /*
        public void MeetTheGround()
        {
            int new_y = Shape.Y;
            int new_x = Shape.X;
            if (speed == 0)
            {
                new_y += cons; //inverse going down := +
                MovingMember help = new MovingMember(); 
                mapa.TryMoveFromTo(this, Shape.X, Shape.Y, new_x, new_y, ref help);
                if (mapa.IsFree(help.Shape))
                {
                    this.Shape = help.Shape;
                    Shape.X = new_x;
                    Shape.Y = new_y;
                }

                else if (mapa.IsEgg(help.Shape))
                {
                    mapa.DeleteEgg();
                    this.Shape = help.Shape;
                    Shape.X = new_x;
                    Shape.Y = new_y;

                }

                else if (mapa.IsLetter(help.Shape))
                {
                    this.Shape = help.Shape;
                    Shape.X = new_x;
                    Shape.Y = new_y;   
                }
            }
            

        }
        */
    }

    public enum PressedButton { no, right, up, left, down };

    class Hero : MovingMember
    {
        //private static bool jump = false; 

        public Hero(Mapa mapa, int wherex, int wherey)
        {
            this.Shape.X = wherex;
            this.Shape.Y = wherey;
            this.mapa = mapa;
        }


        public override void DoStep()
        {
            int new_x = Shape.X;
            int new_y = Shape.Y;
            PressedButton key = mapa.pressedButton;
            
            switchJump = false;

            switch (key)
            {
                case PressedButton.right:
                    speed += cons;
                    break;
                case PressedButton.up: //letter
                    if (letterBool == false)
                    {
                        speedup = 0;
                        if (jump == 0 && mapa.IsGroundRightDown(this.Shape))
                        {
                            speedup = cons;
                            jump = 1;
                        }
                    }
                    else
                    {
                        speedup += consY;
                    }

                    break;
                case PressedButton.left:
                    speed -= cons;
                    break;
                case PressedButton.down: //letter
                    if (letterBool == true)
                    {
                        speedup -= consY;
                    }

                    break;
                case PressedButton.no:
                    if (speed > 0)
                        speed -= (int) (cons / 2); //slower

                    if (letterBool == true && speedup > 0)
                    {
                        speedup -= (int) (consY / 2);
                    }

                    if (speed < 0)
                        speed += (int) (cons / 2);

                    if (letterBool == true && speedup < 0)
                    {
                        speedup += (int) (consY / 2);
                    }

                    break;
            }

            if (speed > maxSpeed)
            {
                speed = maxSpeed;
            }

            if (speed < -maxSpeed)
            {
                speed = -maxSpeed;
            }

            if (speedup > maxSpeedUp)
            {
                speedup = maxSpeedUp;
            }

            if (speedup < -maxSpeedUp)
            {
                speedup = -maxSpeedUp;
            }

            //continious move can be added if wanted by trying one by one pixel as in TryToMoveFrom

            //speedup


            /*
            if (jump != 0) // 0 -> 1 -> 2 -> 3 <- 4 <- 5 -< 0 ... 
            {
                if (jump < 3)
                {
                    speedup = jump * jumpConst;
                    jump++;
                }
                else if (jump < 5)
                {
                    speedup = jumpConst * (2 - jump);
                    jump++;
                }
                else
                {
                    speedup = 0;
                    jump = 0;
                }
            }
            */
            if (letterBool == false)
            {
                
               this.Jump();
                

                new_x = Shape.X + speed;
                new_y = Shape.Y - speedup;

                if (new_x < 0)
                {
                    new_x = 0;
                }

                if (new_x > mapa.SizeX)
                {
                    new_x = mapa.SizeX;
                }

                if (new_y < 0)
                {
                    new_y = 0;
                }

                if (new_y > mapa.SizeY)
                {
                    new_y = mapa.SizeY;
                }

        
                MovingMember MMHelp = new MovingMember();
                mapa.TryMoveFromTo(this, Shape.X, Shape.Y, new_x, new_y, ref MMHelp);

                if (new_x > Shape.X && new_y > Shape.Y)
                {
                    
                }
                
                if (mapa.IsFree(MMHelp.Shape, this))
                {
                    this.Shape = MMHelp.Shape;
                    moved = true;
                    Shape.X = new_x;
                    Shape.Y = new_y;
                }
                else
                {
                    moved = false;
                }

                if (mapa.IsEgg(MMHelp.Shape))
                {
                    mapa.DeleteEgg();

                    //this.Shape = MMHelp.Shape;
                    //Shape.X = new_x;
                    //Shape.Y = new_y;
                }
                if (mapa.isMonster(MMHelp.Shape, this))
                {
                    mapa.state = States.lose;
                    //break;
                }

                if (mapa.IsLetter(MMHelp.Shape) && !mapa.isWall(MMHelp.Shape))
                {
                    this.Shape = MMHelp.Shape;
                    Shape.X = new_x;
                    Shape.Y = new_y;
                    //movedLetter = true;

                    //speed = 0;
                    speedup = 0;

                    letterBool = true;
                    jump = 0;
                }
                
                else
                {
                    letterBool = false;
                }
                
                
                if (mapa.isWall(MMHelp.Shape))
                {
                    speedup = 0;
                    speed = 0;
                }


                mapa.IsGround(this, this.jump);
                
            }

            else
            {

                new_x = Shape.X + speed;
                new_y = Shape.Y - speedup;

                if (new_x < 0)
                {
                    new_x = 0;
                }

                if (new_x > mapa.SizeX)
                {
                    new_x = mapa.SizeX;
                }

                if (new_y < 0)
                {
                    new_y = 0;
                }

                if (new_y > mapa.SizeY)
                {
                    new_y = mapa.SizeY;
                }


                MovingMember MMHelp = new MovingMember();
                mapa.TryMoveFromTo(this, Shape.X, Shape.Y, new_x, new_y, ref MMHelp);

                if (mapa.IsFree(MMHelp.Shape, this))
                {
                    this.Shape = MMHelp.Shape;
                    Shape.X = new_x;
                    Shape.Y = new_y;
                }
                
                if (mapa.IsEgg(MMHelp.Shape))
                {
                    mapa.DeleteEgg();

                    //this.Shape = MMHelp.Shape;
                    //Shape.X = new_x;
                    //Shape.Y = new_y;
                }

                if (mapa.isMonster(MMHelp.Shape, this))
                {
                    mapa.state = States.lose;
                }

                if (mapa.IsLetter(MMHelp.Shape) && !mapa.isWall(MMHelp.Shape))
                {
                    this.Shape = MMHelp.Shape;
                    Shape.X = new_x;
                    Shape.Y = new_y;
                    //speed = 0;
                    //speedup = 0;
                    letterBool = true;
                    jump = 0;
                }


                if (mapa.IsLetter(this.Shape))
                {
                    letterBool = true;
                }
                else
                {
                    letterBool = false;
                }
                
                
                if (mapa.isWall(MMHelp.Shape))
                {
                    speedup = 0;
                    speed = 0;
                }


                //mapa.IsGroundEgg(this, this.jump, false);
            }



        }


        private void Jump()
        {
            int new_x = this.Shape.X + speed;
            int new_y = this.Shape.Y;
            
            if (jump != 0) // 0 -> 1 -> 2 -> 3 <- 4 <- < 0 ... 
            {
                if (jump < 3)
                {
                    speedup = jump * jumpConst;
                    jump++;
                }
                else if (jump < 5)
                {
                    speedup = jumpConst * (2 - jump);
                    jump++;
                }
                else
                {
                    speedup = 0;
                    jump = 0;
                }
            }

            if (jump == 0)
            {
                speedup = 0;
            }

            if (speedup > 0)
            {
                for (int i = 0; i < speedup; i++)
                {
                    new_y--;
                    
                    MovingMember MMHelp = new MovingMember();
                    mapa.TryMoveFromTo(this, Shape.X, Shape.Y, new_x, new_y, ref MMHelp);
                    if (mapa.IsFree(MMHelp.Shape, this))
                    {
                        continue;
                    }
/*
                    if (mapa.IsEgg(MMHelp.Shape))
                    {
                        continue;
                    }
                    */
                    if (mapa.isMonster(MMHelp.Shape, this))
                    {
                        mapa.state = States.lose;
                    }
                    else
                    {
                        new_y++;
                        speedup = this.Shape.Y - new_y;
                        switchJump = true;
                        jump = 0;
                        return;
                    }    
                }

            }
            
            if (speedup < 0)
            {
                for (int i = 0; i > speedup; i--)
                {
                    new_y++;
                    
                    MovingMember MMHelp = new MovingMember();
                    mapa.TryMoveFromTo(this, Shape.X, Shape.Y, new_x, new_y, ref MMHelp);
                    if (mapa.IsFree(MMHelp.Shape, this))
                    {
                        continue;
                    }
/*
                    if (mapa.IsEgg(MMHelp.Shape))
                    {
                        continue;
                    }
                    */
    
                    else
                    {
                        new_y--;
                        speedup = new_y - this.Shape.Y;
                        jump = 0;
                        switchJump = true;
                        return;
                    }    
                }

            }
        }
        
        
    }

    class NotMovingMember : Member
    {
        public NotMovingMember(Mapa mapa, int wherex, int wherey)
        {
            this.mapa = mapa;
            this.Shape.X = wherex;
            this.Shape.Y = wherey;
        }  
        
    }

    class Letter : NotMovingMember
    {
        public Letter(Mapa mapa, int wherex, int wherey) : base(mapa, wherex, wherey)
        {
            
        }
        
              
    }

    class Egg : NotMovingMember
    {
        public Egg(Mapa mapa, int wherex, int wherey) : base(mapa, wherex, wherey)
        {
   
        } 
        
    }

    class Monster : MovingMember
    {
        //private static char[] dirArray = {'>', '^', '<', 'v'};
       
        
        public Monster(Mapa mapa, int wherex, int wherey)
        {
            this.Shape.X = wherex;
            this.Shape.Y = wherey;
            this.mapa = mapa;
        }
        
        private int counter = 0;
        private int counterMove = 0;
        private long groundOrLetter = 0;

        public void GetDualDir()
        {
            switch (direction)
            {
               case '>':
                   direction = '<';
                   break;
               case '<' :
                   direction = '>';
                   break;
               case 'v':
                   direction = '^';
                   break;
               case '^':
                   direction = 'v';
                   break;
               default:
                   break;
                
                
             }
        }

        public bool IsGroundOrLetter()
        {
            if (groundOrLetter < 150)
            {
                return true;
            }
            else
            {
                if (groundOrLetter > 400)
                {
                    groundOrLetter = 0;
                }
                return false;
            }
        }

        public override void DoStep()
        {
            this.speed = 5;
            groundOrLetter++;
            int new_x = Shape.X;
            int new_y = Shape.Y;
            moved = false;


            switch (direction)
            {
                case '>':
                    new_x += speed;
                    break;
                case '^':
                    new_y -= speed;
                    break;
                case '<':
                    new_x -= speed;
                    break;
                case 'v':
                    new_y += speed;
                    break;
            }

            if (!IsGroundOrLetter())
            {
                MovingMember Help = new MovingMember();
                mapa.TryMoveFromTo(this, Shape.X, Shape.Y, new_x, new_y, ref Help);

                if (mapa.IsHalfLetter(Help.Shape))
                {

                    //just left right moving, letters and flying later
                    if (mapa.IsHero(Help.Shape, this))
                    {
                        this.Shape = Help.Shape;
                        Shape.X = new_x;
                        Shape.Y = new_y;
                        mapa.state = States.lose;
                        moved = true;
                    }

                    if (mapa.IsFree(Help.Shape, this))
                    {
                        this.Shape = Help.Shape;
                        Shape.X = new_x;
                        Shape.Y = new_y;
                        moved = true;
                    }

                    if (mapa.IsEgg(Help.Shape))
                    {
                        /*
                        this.Shape = Help.Shape;
                        Shape.X = new_x;
                        Shape.Y = new_y;
                        */
                        mapa.DeleteEgg();
                    }

                    if (counter <= 200)
                    {
                        direction = '^';
                    }

                    if (counter > 200)
                    {
                        direction = 'v';
                        if (counter == 400)
                        {
                            counter = -1; //to sum up to  0
                        }
                    }

                    counter++;



                    if (!mapa.isWall(Help.Shape) && !mapa.isMonster(Help.Shape, this))
                    {

                        this.Shape = Help.Shape;
                        Shape.X = new_x;
                        Shape.Y = new_y;
                        moved = true;
                    }


                    //test -- this.Shape.Y++;
                }
                else
                {
                    if (direction == 'v' || direction == '^')
                    {
                        direction = '>';
                    }

                    if (counterMove == num)
                    {
                        switch (direction)
                        {
                            case '>':
                                direction = '<';
                                break;
                            case '<':
                                direction = '>';
                                break;
                            default:
                                direction = '>';
                                break;
                        }


                        num = rand.Next(500);
                        counterMove = -1;

                    }

                    counterMove++;


                    //just left right moving, letters and flying later
                    if (mapa.IsHero(Help.Shape, this))
                    {
                        this.Shape = Help.Shape;
                        Shape.X = new_x;
                        Shape.Y = new_y;
                        mapa.state = States.lose;
                        moved = true;
                    }

                    if (mapa.IsFree(Help.Shape, this))
                    {
                        this.Shape = Help.Shape;
                        Shape.X = new_x;
                        Shape.Y = new_y;
                        moved = true;
                    }

                    if (mapa.IsEgg(Help.Shape))
                    {
                        /*
                        this.Shape = Help.Shape;
                        Shape.X = new_x;
                        Shape.Y = new_y;
                        */
                        mapa.DeleteEgg();
                    }

                    

                  

                    //test -- this.Shape.Y++;
                    mapa.IsGround(this, jump = 0);
                    
                }
                
            }
            else
            {
                if (direction == 'v' || direction == '^')
                {
                    direction = '>';
                }

                if (counterMove == num)
                {
                    switch (direction)
                    {
                        case '>':
                            direction = '<';
                            break;
                        case '<':
                            direction = '>';
                            break;
                        default:
                            direction = '>';
                            break;
                    }


                    num = rand.Next(500);
                    counterMove = -1;

                }

                counterMove++;

                MovingMember Help = new MovingMember();
                mapa.TryMoveFromTo(this, Shape.X, Shape.Y, new_x, new_y, ref Help);

                //just left right moving, letters and flying later
                if (mapa.IsHero(Help.Shape, this))
                {
                    this.Shape = Help.Shape;
                    Shape.X = new_x;
                    Shape.Y = new_y;
                    mapa.state = States.lose;
                    moved = true;
                }

                if (mapa.IsFree(Help.Shape, this))
                {
                    this.Shape = Help.Shape;
                    Shape.X = new_x;
                    Shape.Y = new_y;
                    moved = true;
                }

                if (mapa.IsEgg(Help.Shape))
                {
                    /*
                    this.Shape = Help.Shape;
                    Shape.X = new_x;
                    Shape.Y = new_y;
                    */
                    mapa.DeleteEgg();
                }

                mapa.IsGround(this, jump = 0);

            }

            if (!moved)
            {
                if (direction == 'v' || direction == '^')
                {
                    if (counter <= 200)
                    {
                        counter = 201;
                    }
                    else
                    {
                        counter = 0;
                    }
                }
                GetDualDir();
            }

        }
        
        //mapa.IsGroundEgg(this, jump, false);
            
        
    }

    public enum States
    {
        win,
        lose,
        run,
        start
        
    };


    class Mapa
    {

        public Mapa(string pathMapa, string pathIkonky, int size_x, int size_y)
        {
            ReadPics(pathIkonky);
            ReadMap(pathMapa);
            state = States.run;
            SizeX = size_x;
            SizeY = size_y;
        }
        List<MovingMember> movingMembers = new List<MovingMember>();
        List<NotMovingMember> notMovingMembers = new List<NotMovingMember>();
        List<Egg> eggs = new List<Egg>();
        List<Letter> letters = new List<Letter>();
        public Hero hero;

        public Bitmap[] pics;

        //private char[,] plan;


        //private static int width = sx;
        //private static int height = sx;

        private static Egg localEgg;

        public int missingEggs = 0; //some number

        public States state;

        public PressedButton pressedButton;

        public int sx;

        public int SizeX, SizeY;


        private int width;
        private int height;

        public bool IsFree(Rectangle rect, MovingMember member)
        {
            //Rectangle rect = member.Shape;
            foreach (MovingMember MM in movingMembers)
            {
                //return false; jj
                if (rect.IntersectsWith(MM.Shape) && MM != member)
                {
                    return false;
                }
            }

            foreach (NotMovingMember MM in notMovingMembers)
            {
                //return false;
                if (rect.IntersectsWith(MM.Shape))
                {
                    return false;
                }
            }
/*
            foreach (Letter L in letters)
            {
                if (rect.IntersectsWith(L.Shape))
                {
                    return false;
                }
            }
            */
            
            /*
            foreach (Egg E in eggs)
            {
                if (rect.IntersectsWith(E.Shape))
                {
                    return false;
                }
            }
            */

            return true;

        }

        public bool isWall(Rectangle rect)
        {
            /*
            foreach (MovingMember MM in movingMembers)
            {
                if (MM.Shape.IntersectsWith(rect))
                {
                    return true;
                }  
            }
            */

            foreach (NotMovingMember nMM in notMovingMembers)
            {
                if (nMM.Shape.IntersectsWith(rect))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsEgg(Rectangle rect)
        {
            foreach (Egg e in eggs)
            {
                if (rect.IntersectsWith(e.Shape))
                {
                    Rectangle overLeap = Rectangle.Intersect(e.Shape, rect);
                    int area = overLeap.Width * overLeap.Height;
                    if (area > 0)//(int) ((width * height) / 2)) // bugg because there is a gap
                    {
                        localEgg = e;
                        return true;
                        //mapa[e.X, e.Y] = nothing;

                    }
                }
            }

            //return false;

            if (missingEggs == 0)
            {
                state = States.win;
            }
            
            return false;
        }

        public void DeleteEgg()
        {
            int i;
            for (i = 0; i < eggs.Count; i++)
            {
                if (localEgg == eggs[i])
                {
                    break;
                }
            }

            eggs.RemoveAt(i);
            missingEggs--;
        }

        public bool IsLetter(Rectangle rect)
        {
            foreach (Letter l in letters)
            {
                if (rect.IntersectsWith(l.Shape))
                {
                    Rectangle overLeap = Rectangle.Intersect(l.Shape, rect); // l ???
                    int area = overLeap.Width * overLeap.Height;
                    if (area > 0)//(int) ((width * height) / 2))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsHero(Rectangle rect, MovingMember MM)
        {
            if (rect.IntersectsWith(hero.Shape) && MM != hero)
            {
                return true;
            }

            return false;
        }

        public void IsGround(MovingMember MM, int jump)
        {
            
            
            
            if (jump > 0 || (MM.switchJump == true))
            {
                return;
            }
            

            int localCons = 30;

            Rectangle rect = MM.Shape;

            int new_x = rect.X;
            int new_y = rect.Y;

            Rectangle rectHelp = new Rectangle();

            /* Copy */
            rectHelp.X = rect.X;
            rectHelp.Y = rect.Y;
            rectHelp.Width = rect.Width;
            rectHelp.Height = rect.Height;
            
            //MovingMember MMHelp = new MovingMember();
            //MM.Shape = rect;
            
            

            for (int i = 1; i <= localCons; i++)
            {
                rectHelp.Y++;

                if (this.IsFree(rectHelp, MM))
                {
                    continue;
                }

  
                else if (this.IsFree(rectHelp,MM) && this.IsHero(rectHelp, MM))
                {
                    state = States.lose;
                }
                else
                {
                    rectHelp.Y--;
                    MoveFromTo(ref MM, rect.X, rect.Y, rectHelp.X, rectHelp.Y);
                    if (this.IsEgg(MM.Shape))
                    {
                        DeleteEgg();
                    }
                    return; //break;
                }

            }

            if (rect.X != rectHelp.X || rect.Y != rectHelp.Y)
            {
                MM.moved = true;
            }
            MoveFromTo(ref MM, rect.X, rect.Y, rectHelp.X, rectHelp.Y);
            if (this.IsEgg(MM.Shape))
            {
                DeleteEgg();
            }

            return;

        }

        public bool IsGroundRightDown(Rectangle rect)
        {
            Rectangle helpRect = new Rectangle();
            helpRect.X = rect.X;
            helpRect.Y = rect.Y;
            helpRect.Width = rect.Width;
            helpRect.Height = rect.Height;

            helpRect.Y++;
            if (isWall(helpRect))
            {
                return true;
            }

            return false;
        }

        public bool isMonster(Rectangle rect, MovingMember member)
        {
            foreach (MovingMember MM in movingMembers)
            {
                if (member != MM && rect.IntersectsWith(MM.Shape))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsHalfLetter(Rectangle rect)
        {
            int sum = 0;
            foreach (Letter L in letters)
            {
                if (rect.IntersectsWith(L.Shape))
                {
                    Rectangle rectHelp = Rectangle.Intersect(rect, L.Shape);
                    int area = rectHelp.Width * rectHelp.Height;
                    sum += area;
                }
            }

            if (sum > (3 * sx * sx) / 4)
            {
                return true;
            }

            return false;
        }

        public void MoveFromTo(ref MovingMember MM, int x, int y, int new_x, int new_y)
        {
            Rectangle rect = new Rectangle();
            
            rect.X = new_x; //left upper
            rect.Y = new_y;
            rect.Width = MM.Shape.Width;
            rect.Height = MM.Shape.Height;
            MM.Shape = rect;

            //return MM; //maybe ref is needed
            //DRAW THE NEW POSITION
            //mapa[rect.X][rect.Y] = rect; //how???

        }
        

        public void TryMoveFromTo(MovingMember MM, int x, int y, int new_x, int new_y, ref MovingMember MMHelp)
        {
            Rectangle rect = new Rectangle();

            rect.X = new_x;
            rect.Y = new_y;
            rect.Width = MM.Shape.Width;
            rect.Height = MM.Shape.Height;
            

            MMHelp.Shape = rect;

        }

        //static int[][] mapa = new int[10000][10000]; //pixels then push to bitmap, or pixel box


        public void ReadPics(string cesta)
        {
            //pics = new Bitmap[15];
            Bitmap bmp = new Bitmap(cesta);
            this.sx = bmp.Height;
            int count = bmp.Width / sx;
            pics = new Bitmap[count];
            for (int i = 0; i < count; i++)
            {
                Rectangle rect = new Rectangle(i * sx, 0, sx, sx);
                pics[i] = bmp.Clone(rect, System.Drawing.Imaging.PixelFormat.DontCare); //second ?
            }

            width = sx;
            height = sx;
        }

        /*
        public void CreateTable(Graphics g, int widthOfPixel, int heightOfPixel) // ?
        {
            int widthOfWindow = widthOfPixel / sx;
            int heightOfWindow = heightOfPixel / sx;

            if (widthOfWindow > width)
            {
                widthOfWindow = width;
            }

            if (widthOfWindow > height)
            {
                widthOfWindow = height;
            }
        }
        */

        public void DeleteMovingMember(MovingMember MM)
        {
            for (int i = 0; i < movingMembers.Count; i++)
            {
                if (MM == movingMembers[i])
                {
                    movingMembers.RemoveAt(i);
                    break;
                }
                //maybe delete from screen, if needed
            }
            
        }

        public void IsGroundEgg(MovingMember MM, int jump, bool monsterbool)
        {
            if (jump > 0)
            {
                return;
            }


            int localCons = sx;

            Rectangle rect = MM.Shape;

            int new_x = rect.X;
            int new_y = rect.Y;

            Rectangle rectHelp = new Rectangle();

            /* Copy */
            rectHelp.X = rect.X;
            rectHelp.Y = rect.Y;
            rectHelp.Width = rect.Width;
            rectHelp.Height = rect.Height;
            
            //MovingMember MMHelp = new MovingMember();
            //MM.Shape = rect;
            
            

            for (int i = 1; i <= localCons; i++)
            {
                rectHelp.Y++;

                if (this.IsEgg(rectHelp))
                {
                    continue;
                }
                else
                {
                    rectHelp.Y--;
                    MoveFromTo(ref MM, rect.X, rect.Y, rectHelp.X, rectHelp.Y);
                    return;
                }

            }
            MoveFromTo(ref MM, rect.X, rect.Y, rectHelp.X, rectHelp.Y);
            IsEgg(MM.Shape);
            if (!monsterbool) 
            {
                DeleteEgg();
            }
            else
            {
                //it thinks there is still egg so it goes down do one so consider egg as air or delete it as well
            }

            return;
        }
        
        /*

        public void MoveFromToConti(MovingMember MM, int x, int y, int new_x, int new_y)
        {
            int dx = new_x - x;
            int dy = new_y - y;
            
            for(int i = 0; i < Math.Min(dx, dy))
        }
        
        */


        public void MoveWithAllMovingMembers(PressedButton pressedButtonInput)
        {
            pressedButton = pressedButtonInput; //repair

            foreach (MovingMember MM in movingMembers)
            {
                MM.DoStep();
            }

            hero.DoStep();
        }

        public void ClearItSelf(Graphics g, int sizeX, int sizeY)
        {
            for (int x = 0; x < sizeX; x += sx)
            {
                for (int y = 0; y < sizeY; y += sx)
                {
                    g.DrawImage(pics[0], x, y, sx, sx);
                }
            }

        }


        public void DrawItSelf(Graphics g)
        {
            //Pen blackPen = new Pen(Color.Black, 3);
            //Pen redPen = new Pen(Color.Red, 3);
            //Pen bluePen = new Pen(Color.Blue, 3);
            /*
            foreach (NotMovingMember nMM in notMovingMembers)
            {
                //g.DrawRectangle(blackPen, nMM.Shape);
            }
            */

            foreach (Letter L in letters)
            {
                g.DrawImage(pics[3], L.Shape.X, L.Shape.Y, sx, sx);
            }

            foreach (Egg E in eggs)
            {
                g.DrawImage(pics[8], E.Shape.X, E.Shape.Y, sx, sx);
            }

            foreach (NotMovingMember nMM in notMovingMembers)
            {
                g.DrawImage(pics[1], nMM.Shape.X, nMM.Shape.Y, sx, sx);
            }
                         
            foreach (MovingMember MM in movingMembers)
            {
                //g.DrawRectangle(redPen, MM.Shape);
                switch (MM.direction)
                {
                    case '>':
                        g.DrawImage(pics[6], MM.Shape.X, MM.Shape.Y, sx, sx);
                        break;
                    case '^':
                        g.DrawImage(pics[5], MM.Shape.X, MM.Shape.Y, sx, sx);
                        break;
                    case '<':
                        g.DrawImage(pics[4], MM.Shape.X, MM.Shape.Y, sx, sx);
                        break;
                    case 'v':
                        g.DrawImage(pics[7], MM.Shape.X, MM.Shape.Y, sx, sx);
                        break;
                    default:
                        break;
                }

            }
            
            //g.DrawRectangle(bluePen, hero.Shape); //consider hero as individual unit
            g.DrawImage(pics[2], hero.Shape.X, hero.Shape.Y, sx, sx);
        }

        public void ReadMap(string cesta)
        {
            //movingMembers = new List<MovingMember>(); ?
            
            System.IO.StreamReader sr = new System.IO.StreamReader(cesta);

            width = int.Parse(sr.ReadLine());
            height = int.Parse(sr.ReadLine());

            //plan = new char[height, width];

            for (int i = 0; i < height; i++)
            {
                string row = sr.ReadLine();

                for (int j = 0; j < width; j++)
                {
                    char c = row[j];
                    Rectangle rect = new Rectangle();

                    //plan[i, j] = c;

                    switch (c)
                    {
                        case 'H':
                            this.hero = new Hero(this, j * sx, i * sx);
                            
                            rect.X = j * sx;
                            rect.Y = i * sx;
                            rect.Width = sx;
                            rect.Height = sx;

                            hero.Shape = rect;
                            break;
                        case '>':
                        case '^':
                        case '<':
                        case 'v':
                            Monster monster = new Monster(this, j * sx, i * sx);

                            monster.direction = c;
                            
                            rect.X = j * sx;
                            rect.Y = i * sx;
                            rect.Width = sx;
                            rect.Height = sx;

                            monster.Shape = rect;

                            movingMembers.Add(monster);

                            break;

                        case 'L':

                            Letter letter = new Letter(this, j * sx, i * sx);
                            
                            rect.X = j * sx;
                            rect.Y = i * sx;
                            rect.Width = sx;
                            rect.Height = sx;

                            letter.Shape = rect;

                            letters.Add(letter);

                            break;

                        case 'E':

                            Egg egg = new Egg(this, j * sx, i * sx);
                            rect.X = j * sx;
                            rect.Y = i * sx;
                            rect.Width = sx;
                            rect.Height = sx;

                            egg.Shape = rect;

                            missingEggs++;

                            eggs.Add(egg);

                            break;
                        
                        case 'X':
                            NotMovingMember nMM = new NotMovingMember(this, j*sx, i*sx);
                            rect.X = j * sx;
                            rect.Y = i * sx;
                            rect.Width = sx;
                            rect.Height = sx;
                            
                            nMM.Shape = rect;
                            
                            notMovingMembers.Add(nMM);

                            break;
                        
                        default:

                            break;

                    }

                }

            }
            
            sr.Close();



        }

    }
}
