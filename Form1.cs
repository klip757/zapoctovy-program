﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace CSHra
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Mapa mapa;
        Graphics g;
        PictureBox pictureBox;
        private void button1_Click(object sender, EventArgs e)
        {
        //    
            g = CreateGraphics(); 
            /*
            Rectangle rect = new Rectangle(0, 0, ClientSize.Width, ClientSize.Height);
            Pen GreenPen = new Pen(Brushes.Green, 3);
            g.DrawRectangle(GreenPen, rect);
                */        
            mapa = new Mapa("plan.txt", "ikonky.png", this.ClientSize.Width, this.ClientSize.Width);
            mapa.state = States.run;
            this.Text = "Zbývá sebrat " + mapa.missingEggs + " vajiček";

            timer1.Enabled = true;
            button1.Visible = false;

            pictureBox = new PictureBox();
            pictureBox.Paint += new PaintEventHandler(this.picturebox_Paint);
            pictureBox.Size = this.ClientSize;
            pictureBox.Visible = true;
            pictureBox.Location = new Point(0, 0);
            pictureBox.Anchor = AnchorStyles.Left;
            //InitializeComponent();
            this.Controls.Add(pictureBox);
            pictureBox.Invalidate();
            pictureBox.Update();
            pictureBox.Refresh();
            //g.DrawImage(mapa.pics[5], 0,0, ClientSize.Width, ClientSize.Height);

            /*
            Rectangle rect = new Rectangle(0, 0, ClientSize.Width, ClientSize.Height);
            Pen GreenPen = new Pen(Brushes.Green, 3);
            g.DrawRectangle(GreenPen, rect);
            g.Dispose();
            */

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            switch (mapa.state)
            {
                case States.run:
                    //g.DrawRectangle();;
                    mapa.MoveWithAllMovingMembers(pressedButton);
                    //mapa.ClearItSelf(g, ClientSize.Width, ClientSize.Height);
                    //this.Text = "Zbývá sebrat " + mapa.missingEggs + " vajíček";
                    this.Text = "WTF" + mapa.hero.speedup + "tak co  letterBOol? " + mapa.hero.letterBool + "dalsi moved + movedLeteer " +
                        mapa.hero.moved + " " + mapa.hero.movedLetter;
                    pictureBox.Invalidate();
                    pictureBox.Update();
                    break;
                case States.win:
                    timer1.Enabled = false;
                    pictureBox.Dispose();
                    MessageBox.Show("Vyhra!");
                    this.Text = "Vyhrál jsi, jsi úžasný";
                    break;
                case States.lose:
                    timer1.Enabled = false;
                    pictureBox.Dispose();
                    MessageBox.Show("Prohra!");
                    this.Text = "Tak to zkus znova";
                    break;
                default:
                    break;
            }
        }

        PressedButton pressedButton = PressedButton.no;

        // HACK na odchyceni stisku sipek
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Up)
            {
                pressedButton = PressedButton.up;
                return true;
            }
            if (keyData == Keys.Down)
            {
                pressedButton = PressedButton.down;
                return true;
            }
            if (keyData == Keys.Left)
            {
                pressedButton = PressedButton.left;
                return true;
            }
            if (keyData == Keys.Right)
            {
                pressedButton = PressedButton.right;
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            pressedButton = PressedButton.no;
        }

        private void picturebox_Paint(object sender, PaintEventArgs e)
        {

            mapa.DrawItSelf(e.Graphics); //client width, client height
        }
    }
}
